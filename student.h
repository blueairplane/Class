/*
 * Class Roster
 * student.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include <string>

#include "degree.h"

using std::string;

class Student {
	public:
		Student();
		Student(string studentID,
				string firstName,
				string lastName,
				string emailAddress,
				int age,
				int daysInCourses[],
				Degree degreeProgram);
		virtual ~Student();

		// accessors:
		const string& getStudentID() const;
		const string& getFirstName() const;
		const string& getLastName() const;
		const string& getEmailAddress() const;
		int getAge() const;
		const int* getDaysInCourses() const;
		virtual Degree getDegreeProgram() const = 0;

		// mutators:
		void setStudentID(const string& studentID);
		void setFirstName(const string& firstName);
		void setLastName(const string& lastName);
		void setEmailAddress(const string& emailAddress);
		void setAge(int age);
		void setDaysInCourses(const int* daysInCourses);
		virtual void setDegreeProgram(Degree degreeProgram) = 0;

		// other methods:
		virtual void print();

	protected:
		Degree degreeProgram;

	private:
		const static int NUM_COURSES = 3;
		string studentID;
		string firstName;
		string lastName;
		string emailAddress;
		int age;
		int daysInCourses[NUM_COURSES];
};

#endif /* STUDENT_H_ */
