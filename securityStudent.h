/*
 * Class Roster
 * securityStudent.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef SECURITYSTUDENT_H_
#define SECURITYSTUDENT_H_

#include "student.h"

class SecurityStudent : public Student {
	public:
		SecurityStudent(string studentID = "",
				string firstName = "",
				string lastName = "",
				string emailAddress = "",
				int age = 0,
				int daysInCourses[] = {},
				Degree degreeProgram = SECURITY);

		// accessors:
		Degree getDegreeProgram() const;

		// mutators:
		void setDegreeProgram(Degree degreeProgram);

		// other methods:
		void print();

	private:
		Degree degreeProgram;
};

#endif /* SECURITYSTUDENT_H_ */
