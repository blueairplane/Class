/*
 * Class Roster
 * roster.cpp
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#include "roster.h"
#include "networkStudent.h"
#include "securityStudent.h"
#include "softwareStudent.h"

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "student.h"

using std::cout;
using std::endl;
using std::string;
using std::istringstream;
using std::vector;

Roster::Roster() {
}

Roster::~Roster() {
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr) {
			delete student;
			this->classRosterArray[i] = nullptr;
		}
	}
}

/*************
 * accessors *
 *************/

Student** Roster::getClassRosterArray() {
	return this->classRosterArray;
}

int Roster::getMaxStudents() {
	return MAX_STUDENTS;
}

/*****************
 * other methods *
 *****************/

void Roster::add(string studentID, string firstName, string lastName,
		string emailAddress, int age, int daysInCourse1, int daysInCourse2,
		int daysInCourse3, Degree degreeProgram) {
	Student* newStudent = nullptr;
	int studentIDNum = studentID.at(1) - '0';
	int daysInCourses[] = {daysInCourse1, daysInCourse2, daysInCourse3};

	if (degreeProgram == SECURITY) {
		newStudent = new SecurityStudent(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram);
	}
	else if (degreeProgram == NETWORKING) {
		newStudent = new NetworkStudent(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram);
	}
	else if (degreeProgram == SOFTWARE) {
		newStudent = new SoftwareStudent(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram);
	}

	this->classRosterArray[studentIDNum - 1] = newStudent;
}

void Roster::remove(string studentID) {
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr && student->getStudentID() == studentID) {
			delete student;
			this->classRosterArray[i] = nullptr;
			return;
		}
	}
	cout << "Student " << studentID << " not found in the class roster." << endl;
}

void Roster::printAll() {
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr) {
			student->print();
			cout << endl;
		}
	}
}

void Roster::printDaysInCourse(string studentID) {
	int sum = 0;
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr && student->getStudentID() == studentID) {
			for (int i = 0; i < 3; ++i) {
				sum += student->getDaysInCourses()[i];
			}
			cout << "Student " << studentID << " averaged "
				 << sum / 3.0 << " days per course." << endl;
		}
	}
}

void Roster::printInvalidEmails() {
	string emailAddress;
	vector<string> invalidEmailAddresses;
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr) {
			emailAddress = student->getEmailAddress();
			if (emailAddress.find("@") == string::npos || emailAddress.find(".") == string::npos || emailAddress.find(" ") != string::npos) {
				invalidEmailAddresses.push_back(emailAddress);
			}
		}
	}
	if (invalidEmailAddresses.size() == 0) {
		cout << "No invalid email addresses." << endl;
	}
	else {
		cout << "Invalid email addresses:" << endl;
		for (unsigned int i = 0; i < invalidEmailAddresses.size(); ++i) {
			cout << "\t" << invalidEmailAddresses.at(i) << endl;
		}
	}
}

void Roster::printByDegreeProgram(int degreeProgram) {
	if (degreeProgram >= 3) {
		cout << "Invalid degree program." << endl;
		return;
	}
	cout << DEGREE_PROGRAM_NAMES[degreeProgram]<< " students:" << endl;;
	for (int i = 0; i < MAX_STUDENTS; ++i) {
		Student* student = this->classRosterArray[i];
		if (student != nullptr && student->getDegreeProgram() == static_cast<Degree>(degreeProgram)) {
			student->print();
			cout << endl;
		}
	}
}

/********
 * main *
 ********/

int main() {
	const int NUM_STUDENTS = 5;
	const string studentData[] = {
			"A1,John,Smith,John1989@gm ail.com,20,30,35,40,SECURITY",
			"A2,Suzan,Erickson,Erickson_1990@gmailcom,19,50,30,40,NETWORK",
			"A3,Jack,Napoli,The_lawyer99yahoo.com,19,20,40,33,SOFTWARE",
			"A4,Erin,Black,Erin.black@comcast.net,22,50,58,40,SECURITY",
			"A5,Brian,Lee,brianlee3742@gmail.com,38,33,11,63,SOFTWARE"
	};
	Roster classRoster;

	// parse studentData & add each student to classRoster:
	for (int i = 0; i < NUM_STUDENTS; ++i) {
		try {
			const string data = studentData[i];

			// temp variables for each student's data:
			string studentID;
			string firstName;
			string lastName;
			string emailAddress;
			int age;
			int daysInCourse1;
			int daysInCourse2;
			int daysInCourse3;
			Degree degreeProgram;

			// temp variables for parsing:
			istringstream iss;
			string temp;

			iss.clear();
			iss.str(data);
			getline(iss, studentID, ',');
			getline(iss, firstName, ',');
			getline(iss, lastName, ',');
			getline(iss, emailAddress, ',');
			getline(iss, temp, ',');
			age = stoi(temp);
			getline(iss, temp, ',');
			daysInCourse1 = stoi(temp);
			getline(iss, temp, ',');
			daysInCourse2 = stoi(temp);
			getline(iss, temp, ',');
			daysInCourse3 = stoi(temp);
			getline(iss, temp);
			if (temp == "SECURITY") {
				degreeProgram = SECURITY;
			}
			else if (temp == "NETWORK") {
				degreeProgram = NETWORKING;
			}
			else if (temp == "SOFTWARE") {
				degreeProgram = SOFTWARE;
			}
			else {
				throw std::runtime_error("Invalid degree program: " + temp);
			}
			classRoster.add(studentID, firstName, lastName, emailAddress, age, daysInCourse1, daysInCourse2, daysInCourse3, degreeProgram);
		}
		catch (std::runtime_error& e) {
			cout << "Error in data: " << e.what() << endl;
			cout << "Student not added." << endl;
		}
	}

	classRoster.printAll();
	classRoster.printInvalidEmails();
	for (int i = 0; i < classRoster.getMaxStudents(); ++i) {
		Student* student = classRoster.getClassRosterArray()[i];
		if (student != nullptr) {
			string studentID = student->getStudentID();
			classRoster.printDaysInCourse(studentID);
		}
	}
	classRoster.printByDegreeProgram(static_cast<int>(SOFTWARE));
	classRoster.remove("A3");
	classRoster.remove("A3");

	// bad practice & unnecessary, but called for in the requirements:
	classRoster.~Roster();

	return 0;
}
