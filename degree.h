/*
 * Class Roster
 * degree.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef DEGREE_H_
#define DEGREE_H_

#include <string>

enum Degree { SECURITY, NETWORKING, SOFTWARE };
const std::string DEGREE_PROGRAM_NAMES[] = {"Security", "Networking", "Software"};

#endif /* DEGREE_H_ */
