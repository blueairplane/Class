/*
 * Class Roster
 * roster.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef ROSTER_H_
#define ROSTER_H_

#include <string>

#include "degree.h"
#include "student.h"

using std::string;

class Roster {
	public:
		Roster();
		~Roster();

		// accessors:
		Student** getClassRosterArray();
		int getMaxStudents();

		// sets the instance variables from part D1 and updates the roster
		void add(string studentID,
				 string firstName,
				 string lastName,
				 string emailAddress,
				 int age,
				 int daysInCourse1,
				 int daysInCourse2,
				 int daysInCourse3,
				 Degree degreeProgram);
		// removes students from the roster by student ID
		// If the student ID does not exist, the function prints an error message
		// indicating that the student was not found
		void remove(string studentID);
		// prints a complete tab-separated list of student data using accessor functions
		// with the provided format:
		// 1 [tab] First Name: John [tab] Last Name: Smith [tab] Age: 20 [tab]daysInCourse: {35, 40, 55} Degree Program: Security
		// The printAll() function should loop through all the students in
		// classRosterArray and call the print() function for each student
		void printAll();
		// correctly prints a student’s average number of days in the three courses
		// The student is identified by the studentID parameter
		void printDaysInCourse(string studentID);
		// verifies student email addresses and displays all invalid email addresses to
		// the user (Note: A valid email should include an at sign ('@') and period ('.')
		// and should not include a space (' '))
		void printInvalidEmails();
		// prints out student information for a degree program specified by an enumerated type
		void printByDegreeProgram(int degreeProgram);

	private:
		const static int MAX_STUDENTS = 5;
		// (array of pointers) to hold the data provided in the studentData table
		Student* classRosterArray[MAX_STUDENTS];
};

#endif /* ROSTER_H_ */
