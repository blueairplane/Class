/*
 * Class Roster
 * student.cpp
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#include <algorithm>
#include <iostream>

#include "student.h"

using std::copy;
using std::cout;

Student::Student() {
	this->studentID = "";
	this->firstName = "";
	this->lastName = "";
	this->emailAddress = "";
	this->age = 0;
	for (int i = 0; i < NUM_COURSES; ++i) {
		this->daysInCourses[i] = 0;
	}
}

Student::Student(string studentID, string firstName, string lastName,
		string emailAddress, int age, int daysInCourses[], Degree degreeProgram) {
	this->studentID = studentID;
	this->firstName = firstName;
	this->lastName = lastName;
	this->emailAddress = emailAddress;
	this->age = age;
	copy(daysInCourses, daysInCourses + NUM_COURSES, this->daysInCourses);
}

Student::~Student() {
}

/*************
 * accessors *
 *************/

const string& Student::getStudentID() const {
	return this->studentID;
}

const string& Student::getFirstName() const {
	return this->firstName;
}

const string& Student::getLastName() const {
	return this->lastName;
}

const string& Student::getEmailAddress() const {
	return this->emailAddress;
}

int Student::getAge() const {
	return this->age;
}

const int* Student::getDaysInCourses() const {
	return this->daysInCourses;
}

/************
 * mutators *
 ************/

void Student::setStudentID(const string &studentID) {
	this->studentID = studentID;
}

void Student::setFirstName(const string &firstName) {
	this->firstName = firstName;
}

void Student::setLastName(const string &lastName) {
	this->lastName = lastName;
}

void Student::setEmailAddress(const string &emailAddress) {
	this->emailAddress = emailAddress;
}

void Student::setAge(int age) {
	this->age = age;
}

void Student::setDaysInCourses(const int *daysInCourses) {
	copy(daysInCourses, daysInCourses + NUM_COURSES, this->daysInCourses);
}

/*****************
 * other methods *
 *****************/

void Student::print() {
	char studentIDNum = getStudentID().at(1);
	cout << studentIDNum << " \t First Name: " << getFirstName()
			             << " \t Last Name: " << getLastName()
						 << " \t Age: " << getAge()
						 << " \tdaysInCourse: {"
						 << getDaysInCourses()[0] << ", "
						 << getDaysInCourses()[1] << ", "
						 << getDaysInCourses()[2] << "}";
}
