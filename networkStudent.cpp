/*
 * Class Roster
 * networkStudent.cpp
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#include <iostream>

#include "networkStudent.h"

using std::cout;

NetworkStudent::NetworkStudent(string studentID, string firstName,
		string lastName, string emailAddress, int age, int daysInCourses[],
		Degree degreeProgram)
: Student(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram)
{
	this->degreeProgram = degreeProgram;
}

/*************
 * accessors *
 *************/

Degree NetworkStudent::getDegreeProgram() const {
	return this->degreeProgram;
}

/************
 * mutators *
 ************/

void NetworkStudent::setDegreeProgram(Degree degreeProgram) {
	this->degreeProgram = degreeProgram;
}

/*****************
 * other methods *
 *****************/

void NetworkStudent::print() {
	Student::print();
	cout << " Degree Program: " << DEGREE_PROGRAM_NAMES[getDegreeProgram()];
}
