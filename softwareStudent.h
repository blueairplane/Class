/*
 * Class Roster
 * softwareStudent.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef SOFTWARESTUDENT_H_
#define SOFTWARESTUDENT_H_

#include "student.h"

class SoftwareStudent : public Student {
	public:
		SoftwareStudent(string studentID = "",
				string firstName = "",
				string lastName = "",
				string emailAddress = "",
				int age = 0,
				int daysInCourses[] = {},
				Degree degreeProgram = SOFTWARE);

		// accessors:
		Degree getDegreeProgram() const;

		// mutators:
		void setDegreeProgram(Degree degreeProgram);

		// other methods:
		void print();

	private:
		Degree degreeProgram;
};

#endif /* SOFTWARESTUDENT_H_ */
