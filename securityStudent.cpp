/*
 * Class Roster
 * securityStudent.cpp
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#include <iostream>

#include "securityStudent.h"

using std::cout;

SecurityStudent::SecurityStudent(string studentID, string firstName,
		string lastName, string emailAddress, int age, int daysInCourses[],
		Degree degreeProgram)
: Student(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram)
{
	this->degreeProgram = degreeProgram;
}

/*************
 * accessors *
 *************/

Degree SecurityStudent::getDegreeProgram() const {
	return this->degreeProgram;
}

/************
 * mutators *
 ************/

void SecurityStudent::setDegreeProgram(Degree degreeProgram) {
	this->degreeProgram = degreeProgram;
}

/*****************
 * other methods *
 *****************/

void SecurityStudent::print() {
	Student::print();
	cout << " Degree Program: " << DEGREE_PROGRAM_NAMES[getDegreeProgram()];
}
