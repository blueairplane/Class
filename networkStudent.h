/*
 * Class Roster
 * networkStudent.h
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#ifndef NETWORKSTUDENT_H_
#define NETWORKSTUDENT_H_

#include "student.h"

class NetworkStudent : public Student {
	public:
		NetworkStudent(string studentID = "",
					   string firstName = "",
					   string lastName = "",
					   string emailAddress = "",
					   int age = 0,
					   int daysInCourses[] = {},
					   Degree degreeProgram = NETWORKING);

		// accessors:
		Degree getDegreeProgram() const;

		// mutators:
		void setDegreeProgram(Degree degreeProgram);

		// other methods:
		void print();

	private:
		Degree degreeProgram;
};

#endif /* NETWORKSTUDENT_H_ */
