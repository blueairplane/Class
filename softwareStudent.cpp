/*
 * Class Roster
 * softwareStudent.cpp
 *
 *      Author: Brian Lee (brianlee3742@gmail.com)
 */

#include <iostream>

#include "softwareStudent.h"

using std::cout;

SoftwareStudent::SoftwareStudent(string studentID, string firstName,
		string lastName, string emailAddress, int age, int daysInCourses[],
		Degree degreeProgram)
: Student(studentID, firstName, lastName, emailAddress, age, daysInCourses, degreeProgram)
{
	this->degreeProgram = degreeProgram;
}

/*************
 * accessors *
 *************/

Degree SoftwareStudent::getDegreeProgram() const {
	return this->degreeProgram;
}

/************
 * mutators *
 ************/

void SoftwareStudent::setDegreeProgram(Degree degreeProgram) {
	this->degreeProgram = degreeProgram;
}

/*****************
 * other methods *
 *****************/

void SoftwareStudent::print() {
	Student::print();
	cout << " Degree Program: " << DEGREE_PROGRAM_NAMES[getDegreeProgram()];
}
